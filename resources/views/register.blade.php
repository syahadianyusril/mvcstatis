<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Sign Up</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up form</h3>
    <form action="/welcome" method="POST">
        @csrf
        <p>First Name:</p>
        <input type="text" name="first">
        <p>Last Name:</p>
        <input type="text" name="last">
        <p>Gender:</p>
        <input type="radio" name="gender">Male    <br>
        <input type="radio" name="gender">Female  <br>
        <input type="radio" name="gender">Other   <br>
        <p>Nationality:</p>
        <select>
            <option>Indonesian</option>
            <option>English</option>
            <option>Chinese</option>
        </select>
        <p>Language Spoken:</p>
        <input type="checkbox" name="language">Bahasa Indonesia  <br>
        <input type="checkbox" name="language">English           <br>
        <input type="checkbox" name="language">Other            <br>
        <p>Bio:</p>
        <textarea cols="30" rows="10"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>