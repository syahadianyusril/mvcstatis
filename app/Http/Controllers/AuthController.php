<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }

    public function welcome(Request $request){
        return view('done');
    }

    public function welcome_post(Request $request){
        $first = $request['first'];
        $last = $request['last'];
        return view('done',['first' => $first],['last' => $last]);
    }
}
